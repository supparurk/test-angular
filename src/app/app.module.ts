import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { TestComponent } from './about/test/test.component';
import { UserModule } from './user/user.module';
import { ProductComponent } from './product/product.component';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    TestComponent,
    ProductComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
